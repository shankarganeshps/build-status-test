#!/bin/sh
bbuild \
-u tim@atlassian.com \
-p $BB_PASSWORD \
-o tpettersen \
-r build-status-test \
-c `git rev-parse HEAD` \
-s $BUILD_STATE \
-k $BUILD_KEY \
-n "A build" \
-l "http://example.com" \
-d "A build description"
